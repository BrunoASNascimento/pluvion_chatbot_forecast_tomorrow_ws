import requests
import json
from datetime import datetime, timedelta
import pandas as pd
from math import radians
from numpy import cos, sin, arcsin, sqrt
import numpy as np
from flask import jsonify
import pluvion_chatbot_scale as scale

url = "https://us-central1-pluvion-tech.cloudfunctions.net/pluvion_chatbot_forecast_get_data/"

power_idw = 2


def get_data(type_fc, lat, lon):
    querystring = {
        "type_fc": type_fc,
        "lat": lat,
        "lon": lon
    }
    response = requests.request(
        "GET", url,  params=querystring)
    data = response.text
    data = json.loads(data)
    print(data[0][0])
    return data


def idw(data_idw, power):
    # data_idw = [[value1,dist1],[value2,dist2]...[valuen,distn]]
    numerator = []
    denominator = []
    for data in data_idw:
        if data[1] == 0:
            return data[0]
        numerator.append(data[0]/(data[1]**power))
        denominator.append(1/data[1]**power)
    z = sum(numerator)/sum(denominator)
    return z


def haversine(row):
    lon1 = row['loc_lat']
    lat1 = row['loc_lon']
    lon2 = row['lon']
    lat2 = row['lat']
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * arcsin(sqrt(a))
    km = 6367 * c
    return km


def parser_data_tomorrow(type_fc, lat, lon):
    datetime_now = datetime.utcnow()
    date_str = datetime_now.date()

    data_array = get_data(type_fc, lat, lon)

    df = pd.DataFrame()

    for data in data_array:
        df = df.append(data)

    datetime_tomorrow = datetime_now + timedelta(days=1)  # date control

    date = datetime_tomorrow.date()  # date control
    print(date)
    df['hour'] = pd.to_datetime(
        df['hour'], format='%Y-%m-%dT%H:%M:%S')

    df['hour(-3h)'] = df['hour'] - timedelta(hours=3)
    df = df[(df['hour(-3h)'].dt.date) == date]  # date control
    df['hours'] = df['hour(-3h)'].dt.hour

    df.loc[df['hours'] <= 11, 'time'] = 'morning'
    df.loc[(df['hours'] > 11) & (df['hours'] <= 18), 'time'] = 'afternoon'
    df.loc[(df['hours'] > 18) | (df['hours'] <= 5), 'time'] = 'night'

    df['hour(-3h)'] = df['hour(-3h)'].dt.date

    df['loc_lat'] = lat
    df['loc_lon'] = lon

    df['dist'] = df.apply(lambda row: haversine(row), axis=1)

    df = df[['hour(-3h)', 'time', 'dist',  'ws']].reset_index()

    # print(df.head(48))
    # print(df.dtypes)
    # print(df.shape)

    df_ed = pd.DataFrame()

    df_ed['ws_array'] = df.groupby(
        by=['hour(-3h)', 'time', 'dist'])['ws'].apply(list)

    df_ed = df_ed.to_json(orient='table')
    df_ed = json.loads(df_ed)

    default = 0
    result = {
        'status': True,
        'data': {
            'D' + str((datetime.strptime(
                val['hour(-3h)'], '%Y-%m-%dT%H:%M:%S.000Z').date()-date_str).days): {}
            for val in df_ed['data']
        }}

    for data in df_ed['data']:
        date_data = datetime.strptime(
            data['hour(-3h)'], '%Y-%m-%dT%H:%M:%S.000Z').date()
        default = (date_data-date_str).days
        name = 'D' + str(default)

        try:

            result['data'][name][data['time']]['ws']['max'].append(
                [np.max(data['ws_array']), data['dist']])

        except:

            ws_data_max = [[np.max(data['ws_array']), data['dist']]]

        data_ed = {
            "date": datetime.strptime(
                data['hour(-3h)'], '%Y-%m-%dT%H:%M:%S.000Z').date(),
            data['time']: {

                "ws": {
                    "max": scale.ws(idw(ws_data_max, power_idw))
                }
            }
        }

        result['data'][name].update(data_ed)

    return result


def pluvion_chatbot_forecast_tomorrow_ws(request):
    json_req = request.args.to_dict(flat=False)
    # url/?fc_type=value&lat=value&lon=value
    fc_type = (json_req['fc_type'])[0]
    lat = round(float((json_req['lat'])[0]), 3)
    lon = round(float((json_req['lon'])[0]), 3)

    if request.method == 'OPTIONS':
        # Allows GET requests from any origin with the Content-Type
        # header and caches preflight response for an 3600s
        headers = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET',
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Max-Age': '3600'
        }

        return ('', 204, headers)

    # Set CORS headers for the main request
    headers = {
        'Access-Control-Allow-Origin': '*'
    }

    try:
        #parser_data_today("SD10DM", -23.55367, -46.65934)
        data = parser_data_tomorrow(fc_type, lat, lon)
    except:
        data = {"status": False}

    return (jsonify(data), 200, headers)


#print(parser_data_today("SD10DM", -23.55367, -46.65934))
